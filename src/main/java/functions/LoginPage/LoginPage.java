package functions.LoginPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;

import functions.BasicPage;

public class LoginPage extends BasicPage {

	PinScreen PS = new PinScreen();

	// Alert Box asking for downloading content at first time of application
	// launch
	public static final By User_Name = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIATextField[@name='Email']");
	public static final By User_Password = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[2]/UIASecureTextField[@name='Password']");
	public static final By Server = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[@name='Server']");
	public static final By ServerURL = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[3]/UIATextField[1]");
	public static final By ServerDone = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[@name='Done']");

	public static final By EulaCheckBox = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[1]");

	public static final By SignIN_Button = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[@name='Sign In']");

	public static final By PasswordShow = By
			.id("com.dell.securelifecycle:id/password_showhide_tv");

	public static final By LoginLogo = By
			.id("com.dell.securelifecycle:id/login_logo");

	public static final By InvalidAlertHedder = By
			.id("com.dell.securelifecycle:id/sl_alertTitle");
	public static final By InvalidLoginMessage = By
			.id("com.dell.securelifecycle:id/sl_alert_message");
	public static final By InvalidLoginOKButton = By
			.id("com.dell.securelifecycle:id/okButton");

	public static final By Pin1 = By
			.xpath("//UIAApplication[1]/UIAWindow[4]/UIAButton[1]");

	public void Invalid_Login(String Invalid_Username, String Invalid_Password,
			String Server_URL) throws InterruptedException {

		mDriver.findElement(User_Name).sendKeys(Invalid_Username);
		mDriver.findElement(User_Password).sendKeys(Invalid_Password);
		WebElement URL = mDriver.findElement(Server);
		URL.click();
		mDriver.findElement(ServerURL).sendKeys(Server_URL);

		mDriver.findElement(ServerDone).click();
		mDriver.findElement(EulaCheckBox).click();
		mDriver.findElement(SignIN_Button).click();

		System.out.println(mDriver.findElement(InvalidLoginMessage).getText());
		mDriver.findElement(InvalidLoginOKButton).click();
		Thread.sleep(1000);
		mDriver.findElement(User_Name).clear();
		Thread.sleep(1000);
		try {
			mDriver.hideKeyboard();
		} catch (Exception e) {
		}

	}

	public void Valid_Login(String Username, String Password, String Server_URL)
			throws InterruptedException {

		// Entering Username
		mDriver.findElement(User_Name).sendKeys(Username);

		// Entering Password
		mDriver.findElement(User_Password).sendKeys(Password);

		// Tapping on Server URL
		WebElement URL = mDriver.findElement(Server);
		URL.click();
		mDriver.findElement(ServerURL).sendKeys(Server_URL); // Entering Server
																// URL

		// Tapping on Done button
		mDriver.findElement(ServerDone).click();

		// Tapping on EULA checkbox
		mDriver.findElement(EulaCheckBox).click();

		// Tapping on SignIn button
		mDriver.findElement(SignIN_Button).click();

		Assert.assertTrue(PS.isPINScreenPresent(),
				"[FAIL] Pin Screen not appearing");
		Reporter.log("[PASS] Pin Screen appearing.");
	}

}
