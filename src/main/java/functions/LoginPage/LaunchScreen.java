package functions.LoginPage;

import org.openqa.selenium.By;
import functions.BasicPage;

public class LaunchScreen extends BasicPage {

	// Alert Box asking for downloading content at first time of application
	// launch
	public static final By AlertBoxTitle = By
			.xpath("//UIAApplication[1]/UIAWindow[7]/UIAAlert[1]/UIAScrollView[1]/UIAStaticText[1]");
	public static final By AlertBoxMessage = By
			.xpath("//UIAApplication[1]/UIAWindow[7]/UIAAlert[1]/UIAScrollView[1]/UIAStaticText[2]");
	public static final By AlertBoxOKButton = By
			.xpath("//UIAApplication[1]/UIAWindow[7]/UIAAlert[1]/UIACollectionView[1]/UIACollectionCell[2]/UIAButton[@name ='OK']");

	public void VerifyAlertBoxMessage() throws InterruptedException {
		// System.out.println(mDriver.currentActivity());
		// System.out.println("Func BoxMessage Verify IN");
		// mDriver.findElement(By.id("android:id/alertTitle")
		System.out.println(mDriver.findElement(AlertBoxTitle).getText());
		System.out.println(mDriver.findElement(AlertBoxMessage).getText());
		Thread.sleep(1000);
		// System.out.println("Func BoxMessage Verify OUT");

	}

	public void ClickOKButton() throws InterruptedException {

		System.out.println(mDriver.findElement(AlertBoxOKButton).getText());
		Thread.sleep(1000);
		mDriver.findElement(AlertBoxOKButton).click();
		System.out.println("OK CLicked");

	}

}