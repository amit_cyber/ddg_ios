package functions.Settings;

import org.openqa.selenium.By;

import functions.BasicPage;

public class AboutScreen extends BasicPage {

	// Menu button
	public static final By Menu_Button = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[contains(@name, 'nav')]");

	// Settings button
	public static final By Settings_Button = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[@name='settings']");
	
	

}
