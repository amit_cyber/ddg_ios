package functions;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeSuite;

public class Set_Driver_Set_Capabilities {
	static IOSDriver mDriver;

	@BeforeSuite
	public void setDesiredCapabilities() throws InterruptedException,
			MalformedURLException {

		// File file = new
		// File("C://Users//vaibhav.kushwaha//Desktop//data//Workspace//magb-qa-android//resources//datafile.properties");

		String dir = System.getProperty("user.dir");
		File file = new File(dir + "//datafile.properties");

		FileInputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		Properties prop = new Properties();

		// load properties file
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			e.printStackTrace();
		}

		File mApplication = new File(prop.getProperty("App_Path"));
		DesiredCapabilities mCapabilities = new DesiredCapabilities();

		// This capability is only required for device which has android older
		// than 4.4(kitkat) - start.
		mCapabilities.setCapability("DEVICE", prop.getProperty("Device"));
		mCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,
				prop.getProperty("Automation_Name"));

		// This capability is only required for device which has android older
		// than 4.4(kitkat) - end.
		mCapabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
		mCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,
				prop.getProperty("PLATFORM_NAME"));
		mCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION,
				prop.getProperty("PLATFORM_VERSION"));
		mCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME,
				prop.getProperty("DEVICE_NAME"));
		mCapabilities.setCapability(MobileCapabilityType.APP,
				mApplication.getAbsolutePath());

		mCapabilities.setCapability(MobileCapabilityType.APP_PACKAGE,
				prop.getProperty("APP_PACKAGE"));
		// mCapabilities.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY,
		// prop.getProperty("APP_WAIT_ACTIVITY"));
		// mCapabilities.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY,"com.magic.launchpad.HomeActivity");
		mCapabilities.setCapability(MobileCapabilityType.APP_ACTIVITY,
				prop.getProperty("APP_ACTIVITY"));

		// Test Object
		// mDriver = new AndroidDriver(new
		// URL("https://app.testobject.com:443/api/appium/wd/hub"),
		// mCapabilities);

		// Local System
		mDriver = new IOSDriver(new URL(prop.getProperty("Appium_Server_URL")),
				mCapabilities);
		mDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		BasicPage bp = new BasicPage();
		bp.setDriver(mDriver);

	}
}