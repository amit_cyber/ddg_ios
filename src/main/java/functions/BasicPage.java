package functions;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;


public class BasicPage 
{
	public static IOSDriver mDriver;
	//public static DesiredCapabilities mCapabilities;
	
	public  void setDriver(IOSDriver mDriver)
	{ 
			this.mDriver = mDriver;

	}
		
	public void setDriverAndDevice(IOSDriver mDriver)
	{
		setDriver(mDriver);
	
	}
	
	
	public boolean isElementPresent(By object_locator)
	{
		try
		{
			mDriver.findElement(object_locator);
			System.out.println();
			return true;
		} 
		catch (NoSuchElementException e)
		{
			return false;
		}
	}
	
	
}