package functions.HomePageFunctions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import functions.BasicPage;

public class HomeScreen extends BasicPage 
{
	@AndroidFindBy(id = "edt1")
	public WebElement edt1;

// File Manager Title
	public static final By fileManager_Title = By.id("android:id/action_bar_title");

//Main Menu Button
	public static final By mainMenu	= By.id("android:id/up");
	
//File View
	public static final By fileView	= By.id("com.dell.securelifecycle:id/action_file_view_mode");

//Filter View	
	public static final By filterSort = By.id("com.dell.securelifecycle:id/action_file_filter_sort");
	
//Search Button
	public static final By search =	By.id("com.dell.securelifecycle:id/action_file_search");
	
//Create New (+) Button	
	public static final By action_Create_new = By.id("com.dell.securelifecycle:id/action_file_create_new");
	
//Documents Default Folder
	public static final By documents = By.xpath("//android.widget.TextView[@resource-id='com.dell.securelifecycle:id/file_item_filename' and @text='Documents']");

//Downloads Default Folder
	public static final By downloads = By.xpath("//android.widget.TextView[@resource-id='com.dell.securelifecycle:id/file_item_filename' and @text='Downloads']");

//Photos Default Folder
	public static final By photos = By.xpath("//android.widget.TextView[@resource-id='com.dell.securelifecycle:id/file_item_filename' and @text='Photos']");	
		

	
	
//Verify presence of Search Button
	public boolean isSearchButtonPersent() 
	{
		By object_locator = search;
		boolean objectSearchResult = isElementPresent(object_locator);
		return objectSearchResult;
	}

//Click on Search Button
	public void clickSearchButton()
	{
		WebElement SearchButton = mDriver.findElement(search);
		SearchButton.click();
	}

//Verify presence of Documents Folder
	public boolean isDocumentsFolderPersent() 
	{
		By object_locator = documents;
		boolean objectSearchResult = isElementPresent(object_locator);
		return objectSearchResult;
	}

//Click on Documents Folder
	public void clickDocumentsFolder()
	{
		WebElement DocumentsFolder = mDriver.findElement(documents);
		DocumentsFolder.click();
	}
	
//Verify presence of Downloads Folder
	public boolean isDowloadsFolderPersent() 
	{
		By object_locator = downloads;
		boolean objectSearchResult = isElementPresent(object_locator);
		return objectSearchResult;
	}

//Click on Downloads Folder
	public void clickDownloadsFolder()
	{
		WebElement DownloadsFolder = mDriver.findElement(downloads);
		DownloadsFolder.click();
	}	

//Verify presence of Photos Folder
	public boolean isPhotosFolderPersent() 
	{
		By object_locator = photos;
		boolean objectSearchResult = isElementPresent(object_locator);
		return objectSearchResult;
	}

//Click on Photos Folder
	public void clickPhotosFolder()
	{
		WebElement PhotosFolder = mDriver.findElement(photos);
		PhotosFolder.click();
	}	
	
// Quit app
		public void quit_app()
		{
			mDriver.closeApp();
			mDriver.quit();
			
		}
	
}
