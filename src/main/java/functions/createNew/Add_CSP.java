package functions.createNew;

import io.appium.java_client.MobileBy.ByIosUIAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByName;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import functions.BasicPage;

public class Add_CSP extends BasicPage {

	// Declaring Explicit Wait
	public static WebDriverWait wait;

	// Create New (+) Button
	public static final By action_Create_new = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[5]");

	// Create New Dialogue
	public static final By create_New = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[5]");

	// Add CSP button
	public static final By action_Add_CSP = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIACollectionView[2]/UIACollectionCell[6]");

	// Add Box Drive
	public static final By add_BoxDrive = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[3]");

	// Box Drive Elements
	public static final By Box_username = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[1]");
	public static final By Box_password = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIASecureTextField[1]");
	public static final By Box_signIn = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]");
	public static final By Box_grantaccess = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]");

	// Add One Drive Personal
	public static final By add_OneDrivePersonal = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[4]");

	// One Drive Personal Elements
	public static final By OneDrivePersonal_username = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[1]");
	public static final By OneDrivePersonal_next = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]");
	public static final By OneDrivePersonal_password = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIASecureTextField[1]");
	public static final By OneDrivePersonal_signIn = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]");

	// Add DropBox Personal
	public static final By add_DropboxPersonal = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]");

	// DropBox Personal Elements
	public static final By DropBoxPersonal_username = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[1]");
	public static final By DropBoxPersonal_password = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIASecureTextField[1]");
	public static final By DropBoxPersonal_signIn = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]");

	// Add GoogleDrive Personal
	public static final By add_GoogleDrivePersonal = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[@name='GoogleDrive Personal']/UIAStaticText[1]");

	public static final By GoogleDriveName = ByIosUIAutomation
			.name("GoogleDrive Personal");
	// By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[6]");
	public static final By GoogleDrive_username = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[1]");
	public static final By GoogleDrive_password = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIASecureTextField[1]");
	public static final By GoogleDrive_SignIn = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]");
	public static final By GoogleDrive_Allow = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[2]");

	// Verify presence of Create New
	public boolean isCreateNewDialoguePersent() {
		By object_locator = create_New;
		boolean objectSearchResult = isElementPresent(object_locator);
		return objectSearchResult;
	}

	// Click on Add Button
	public void clickAddButton() {
		WebElement SearchButton = mDriver.findElement(action_Create_new);
		SearchButton.click();
	}

	// Click on Cloud Service
	public void clickCloudService() {
		WebElement addCSP = mDriver.findElement(action_Add_CSP);
		addCSP.click();

	}

	// enter email for Box login
	public void enter_box_email(String email) {
		WebElement SearchButton = mDriver.findElement(Box_username);
		SearchButton.click();
		SearchButton.sendKeys(email);
	}

	// enter email for Box login
	public void enter_box_password(String password) {
		WebElement SearchButton = mDriver.findElement(Box_password);
		SearchButton.click();
		SearchButton.sendKeys(password);
	}

	// enter email for Box login
	public void enter_box_grantAccess() {
		WebElement SearchButton = mDriver.findElement(Box_signIn);
		SearchButton.click();

	}

	// Add Box Drive
	public void addBoxDrive(String username, String password) {

		wait = new WebDriverWait(mDriver, 30);
		WebElement addBoxDrive = mDriver.findElement(add_BoxDrive);
		addBoxDrive.click();

		wait.until(ExpectedConditions.elementToBeClickable(Box_signIn));

		WebElement Boxusername = mDriver.findElement(Box_username);
		Boxusername.click();
		Boxusername.sendKeys(username);

		WebElement Boxpassword = mDriver.findElement(Box_password);
		Boxpassword.click();
		Boxpassword.sendKeys(password);
		mDriver.findElement(Box_signIn).click();

		wait.until(ExpectedConditions.elementToBeClickable(Box_grantaccess));
		mDriver.findElement(Box_grantaccess).click();

	}

	// Add DropBox Personal
	public void addDropBoxPersonal(String username, String password) {
		wait = new WebDriverWait(mDriver, 30);

		WebElement addDropBoxPersonal = mDriver
				.findElement(add_DropboxPersonal);
		addDropBoxPersonal.click();

		wait.until(ExpectedConditions
				.elementToBeClickable(DropBoxPersonal_signIn));

		mDriver.findElement(DropBoxPersonal_username).sendKeys(username);
		mDriver.findElement(DropBoxPersonal_password).sendKeys(password);
		mDriver.findElement(DropBoxPersonal_signIn).click();
	}

	// Add OneDrive Personal
	public void add_OneDrivePersonal(String username, String password) {
		wait = new WebDriverWait(mDriver, 30);

		WebElement add_OneDrive = mDriver.findElement(add_OneDrivePersonal);
		add_OneDrive.click();

		WebElement addOneDrivePersonal = mDriver
				.findElement(OneDrivePersonal_username);
		addOneDrivePersonal.sendKeys(username);

		wait.until(ExpectedConditions
				.elementToBeClickable(OneDrivePersonal_signIn));

		mDriver.findElement(OneDrivePersonal_password).sendKeys(password);
		mDriver.findElement(OneDrivePersonal_signIn).click();
	}

	// Add Google Drive
	public void add_GoogleDrive(String username, String password) {

		wait = new WebDriverWait(mDriver, 30);

		// UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[@contains='GoogleDrive
		// Personal']/UIAStaticText[1]

		// tapping on Google Drive CSP
		WebElement addGoogleDrive = mDriver
				.findElement(add_GoogleDrivePersonal);
		addGoogleDrive.click();

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(GoogleDrive_username));
		// Entering username for Google Drive
		mDriver.findElement(GoogleDrive_username).sendKeys(username);

		wait.until(ExpectedConditions.elementToBeClickable(GoogleDrive_SignIn));

		mDriver.findElement(GoogleDrive_password).sendKeys(password);

		// tapping on SignIn button
		mDriver.findElement(GoogleDrive_SignIn).click();

		wait.until(ExpectedConditions.elementToBeClickable(GoogleDrive_Allow));

		// allowing Google Drive Sigin
		mDriver.findElement(GoogleDrive_Allow);

	}

}
