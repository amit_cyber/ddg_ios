package functions.createNew;

import org.openqa.selenium.By;

import functions.BasicPage;
import functions.HomePageFunctions.HomeScreen;
import functions.LoginPage.PinScreen;

public class CreateSpreadSheet extends BasicPage {

	HomeScreen HS_Obj = new HomeScreen();
	Add_CSP add_obj = new Add_CSP();
	CreateNewMenu CN_obj = new CreateNewMenu();
	PinScreen ps = new PinScreen();

	// Select Spreadsheet from the sub-menu
	public static final By create_Spreadsheet = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIACollectionView[2]/UIACollectionCell[@name='Spreadsheet']/UIAStaticText[1]");

	public static final By edit_Spreadsheet = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAImage[12]/UIATextView[1]");

	public void Spreadsheet() {

		// tapping on Add new button
		CN_obj.clickAddButton();

		// tapping on Spreadsheet button
		CN_obj.clickCreateSpreadsheetButton();
	}
}
