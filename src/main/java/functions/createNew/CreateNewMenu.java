package functions.createNew;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import functions.BasicPage;

public class CreateNewMenu extends BasicPage {

	// Create New (+) Button
	public static final By action_Create_new = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[5]");

	// Create New Dialogue
	public static final By create_New = By
			.xpath("//android.widget.TextView[@resource-id='android:id/content'/android.widget.LinearLayout/android.widget.TextView and @text='Create New']");

	// Create New Documents
	public static final By create_New_Documents = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIACollectionView[2]/UIACollectionCell[@name='Document']/UIAStaticText[1]");

	// Create New Spreadsheet
	public static final By create_New_Spreadsheet = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIACollectionView[2]/UIACollectionCell[@name='Spreadsheet']/UIAStaticText[1]");

	// Create New Persentation
	public static final By create_New_Persentation = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIACollectionView[2]/UIACollectionCell[@name='Presentation']/UIAStaticText[1]");

	// Documents Blank Template
	public static final By select_Blank_Template = By
			.xpath("//android.widget.ImageView[@resource-id='com.dell.securelifecycle:id/fm_template_thumbnail']");

	// Enter Test on Doc
	// public static final By enterText =
	// By.xpath("//android.widget.TextView[@resource-id='com.dell.securelifecycle:id/slidem_listitem_name']");
	public static final By enterText = By
			.xpath("//android.view.View[@resource-id='com.dell.securelifecycle:id/EvBaseView']");

	// Document Save Menu Button
	public static final By document_SaveMenu = By
			.xpath("//android.widget.ImageButton[@resource-id='com.dell.securelifecycle:id/actionbar_save']");

	// Document Save Button
	public static final By document_SaveButton = By
			.xpath("//android.widget.TextView[@resource-id='com.dell.securelifecycle:id/menu_actionbar_title' and @text='Save']");

	// Document Save As Button
	public static final By document_SaveAsButton = By
			.xpath("//android.widget.TextView[@resource-id='com.dell.securelifecycle:id/menu_actionbar_title' and @text='Save as']");

	// Document Export Button
	public static final By document_ExportButton = By
			.xpath("//android.widget.TextView[@resource-id='com.dell.securelifecycle:id/menu_actionbar_title' and @text='Export']");

	// Document Exit Button
	public static final By document_ExitButton = By
			.xpath("//android.widget.TextView[@resource-id='com.dell.securelifecycle:id/menu_actionbar_title' and @text='Exit']");

	// Document Save Menu Back Button
	public static final By document_SaveMenuBackButton = By
			.xpath("//android.widget.TextView[@resource-id='com.dell.securelifecycle:id/menu_actionbar_image']");

	// Add CSP button
	public static final By action_Add_CSP = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIACollectionView[2]/UIACollectionCell[6]");

	// Add Box Drive
	public static final By add_BoxDrive = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[3]");

	// Add One Drive Personal
	public static final By add_OneDrivePersonal = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[4]");

	// Add DropBox Personal
	public static final By add_DropboxPersonal = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]");

	// Add GoogleDrive Personal
	public static final By add_GoogleDrivePersonal = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[6]");

	// Verify presence of Create New
	public boolean isCreateNewDialoguePersent() {
		By object_locator = create_New;
		boolean objectSearchResult = isElementPresent(object_locator);
		return objectSearchResult;
	}

	// Click on Add Button
	public void clickAddButton() {
		WebElement SearchButton = mDriver.findElement(action_Create_new);
		SearchButton.click();
	}

	// Click on Add CSP button () {
	public void addCSP() {
		WebElement addCSP = mDriver.findElement(action_Add_CSP);
		addCSP.click();
	}

	// Click on Create Documents Button
	public void clickCreateDocumentsButton() {
		WebElement SearchButton = mDriver.findElement(create_New_Documents);
		SearchButton.click();
	}

	// Click on Create Spreadsheet Button
	public void clickCreateSpreadsheetButton() {
		WebElement SearchButton = mDriver.findElement(create_New_Spreadsheet);
		SearchButton.click();
	}

	// Click on Create Persentation Button
	public void clickCreatePersentationButton() {
		WebElement SearchButton = mDriver.findElement(create_New_Persentation);
		SearchButton.click();
	}

	// Click on Select Blank Template on Documents
	public void clickDocumentsBlankTemplate() {
		WebElement SearchButton = mDriver.findElement(select_Blank_Template);
		SearchButton.click();
	}

	// Send test to documents
	public void enterTextToDocument() {
		WebElement SearchButton = mDriver.findElement(enterText);
		SearchButton.click();
		SearchButton.sendKeys("Dummy Text");
	}

	// Click on Save Menu Button
	public void clickSaveMenuButton() {
		WebElement SearchButton = mDriver.findElement(document_SaveMenu);
		SearchButton.click();
	}

	// Click on Save Button
	public void clickSaveButton() {
		WebElement SearchButton = mDriver.findElement(document_SaveButton);
		SearchButton.click();
	}

	public void closeApp() {
		mDriver.closeApp();
	}

	public void launchApp() {
		mDriver.launchApp();
	}

}