package functions.createNew;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import functions.BasicPage;
import functions.HomePageFunctions.HomeScreen;
import functions.LoginPage.PinScreen;

public class CreateDocument extends BasicPage {

	HomeScreen HS_Obj = new HomeScreen();
	Add_CSP add_obj = new Add_CSP();
	CreateNewMenu CN_obj = new CreateNewMenu();
	PinScreen ps = new PinScreen();

	public static WebDriverWait wait;

	// Select Document from the sub-menu
	public static final By create_Document = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIACollectionView[2]/UIACollectionCell[@name='Document']/UIAStaticText[1]");

	// Menu button
	public static final By Document_menu = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[contains(@name, 'menu')]");

	// Back button
	public static final By Document_back = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[contains(@name, 'back')]");

	public static final By Document_save = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAAlert[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[contains(@name, 'Save')]");

	public static final By Document_name = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAAlert[1]/UIAScrollView[1]/UIACollectionView[1]/UIACollectionCell[1]");

	public static final By canvas = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]");

	public void Document() throws InterruptedException {

		// tapping on Add new button.
		CN_obj.clickAddButton();

		// tapping on Document button
		CN_obj.clickCreateDocumentsButton();

		// tapping on the canvas to enable to editor.
		WebElement Canvas = mDriver.findElement(canvas);
		Canvas.click();

		// Typing in the document.
		mDriver.executeScript("UIATarget.localTarget().frontMostApp().keyboard().typeString('hello')");

		// tapping on Menu button.
		mDriver.findElement(Document_menu).click();

		// tapping on Save button
		mDriver.executeScript("UIATarget.localTarget().frontMostApp().mainWindow().scrollViews()[1].tableViews()[0].tapWithOptions({tapOffset:{x:0.16, y:0.10}})");

		// tapping on Save in the pop-up menu.
		mDriver.findElement(Document_save).click();
	}

}
