package smoke.CreateFiles;

import org.testng.annotations.Test;

import functions.Set_Driver_Set_Capabilities;
import functions.HomePageFunctions.HomeScreen;
import functions.LoginPage.PinScreen;
import functions.createNew.Add_CSP;
import functions.createNew.CreateNewMenu;

public class test123 extends Set_Driver_Set_Capabilities {

	HomeScreen HS_Obj = new HomeScreen();
	Add_CSP add_obj = new Add_CSP();
	CreateNewMenu CN_obj = new CreateNewMenu();
	PinScreen ps = new PinScreen();

	@Test(priority = 1)
	public void closeApp() throws InterruptedException {

		ps.EnterPin();
		Thread.sleep(4000);
		CN_obj.closeApp();
		System.out.println("App closed");

	}

	@Test(priority = 2)
	public void launchApp() {
		CN_obj.launchApp();
		System.out.println("App Launched");
	}

}
