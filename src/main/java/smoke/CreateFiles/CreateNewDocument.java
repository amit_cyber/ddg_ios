package smoke.CreateFiles;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import functions.Set_Driver_Set_Capabilities;
import functions.HomePageFunctions.HomeScreen;
import functions.LoginPage.PinScreen;
import functions.createNew.Add_CSP;
import functions.createNew.CreateDocument;
import functions.createNew.CreateNewMenu;

public class CreateNewDocument extends Set_Driver_Set_Capabilities {

	HomeScreen HS_Obj = new HomeScreen();
	Add_CSP add_obj = new Add_CSP();
	CreateNewMenu CN_obj = new CreateNewMenu();
	PinScreen ps = new PinScreen();
	CreateDocument cd = new CreateDocument();

	public static final By create_Document = By
			.xpath("//UIAApplication[1]/UIAWindow[1]/UIACollectionView[2]/UIACollectionCell[@name='Document']/UIAStaticText[1]");

	@Test(priority = 1)
	public void PIN() throws InterruptedException {
		ps.EnterPin();
	}

	@Test(priority = 2)
	public void Document() throws InterruptedException {

		cd.Document();

	}

}
