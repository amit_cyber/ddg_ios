package smoke.HomePageTests;

import org.testng.Assert;
import org.testng.annotations.Test;

import functions.HomePageFunctions.HomeScreen;
import functions.createNew.CreateNewMenu;

public class HomeScreenTests {
	HomeScreen HS_obj = new HomeScreen();
	CreateNewMenu CN_obj = new CreateNewMenu();

	@Test(priority = 1)
	public void SearchButtonPersence() throws InterruptedException {
		HS_obj.isSearchButtonPersent();
	}

	@Test(priority = 2)
	public void DocumentsFolderPersence() throws InterruptedException {
		HS_obj.isDocumentsFolderPersent();
	}

	@Test(priority = 3)
	public void DownloadsFolderPersence() throws InterruptedException {
		HS_obj.isDowloadsFolderPersent();
	}

	@Test(priority = 4)
	public void PhotosFolderPersence() throws InterruptedException {
		HS_obj.isPhotosFolderPersent();
	}

	@Test(priority = 5)
	public void DocumentsClick() throws InterruptedException {
		HS_obj.clickDocumentsFolder();
	}

	@Test(priority = 6)
	public void ClickAddButton() throws InterruptedException {
		CN_obj.clickAddButton();
	}

	@Test(priority = 7)
	public void CreateDocuments() throws InterruptedException {
		CN_obj.clickCreateDocumentsButton();
		Thread.sleep(100);
		CN_obj.clickDocumentsBlankTemplate();
		Thread.sleep(100);
		CN_obj.enterTextToDocument();
		Thread.sleep(100);
		CN_obj.clickSaveMenuButton();
		Thread.sleep(100);

	}

	// @Test (priority=8)
	public void Quit() {
		HS_obj.quit_app();
	}

}
