package smoke.addCSP;

import org.testng.annotations.Test;

import functions.Set_Driver_Set_Capabilities;
import functions.HomePageFunctions.HomeScreen;
import functions.createNew.Add_CSP;
import functions.createNew.CreateNewMenu;

public class AddOneDrivePersonal extends Set_Driver_Set_Capabilities {

	HomeScreen HS_Obj = new HomeScreen();
	Add_CSP add_obj = new Add_CSP();
	CreateNewMenu CN_obj = new CreateNewMenu();

	@Test(priority = 1)
	public void addCSP() {
		// Click (+) button
		CN_obj.clickAddButton();

		// Click on Cloud Service button
		CN_obj.addCSP();
	}

	@Test(priority = 2)
	public void addOneDrivePersonal() {
		// logging into One Drive Personal
		add_obj.add_OneDrivePersonal("Testcguser@outlook.com", "cybergroup@123");

	}

}
