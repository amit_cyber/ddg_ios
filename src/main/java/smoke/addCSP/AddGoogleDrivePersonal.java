package smoke.addCSP;

import org.testng.annotations.Test;

import functions.Set_Driver_Set_Capabilities;
import functions.HomePageFunctions.HomeScreen;
import functions.LoginPage.PinScreen;
import functions.createNew.Add_CSP;
import functions.createNew.CreateNewMenu;

public class AddGoogleDrivePersonal extends Set_Driver_Set_Capabilities {

	HomeScreen HS_obj = new HomeScreen();
	CreateNewMenu CN_obj = new CreateNewMenu();
	Add_CSP add_obj = new Add_CSP();
	PinScreen ps = new PinScreen();

	@Test(priority = 1)
	public void addGoogleDrive() throws InterruptedException {

		// Entering PIN to access app
		ps.EnterPin();

		// click (+) button
		CN_obj.clickAddButton();

		// Click on Cloud Service button
		CN_obj.addCSP();
	}

	@Test(priority = 2)
	public void loginGoogleDrive() {

		// entering username
		add_obj.add_GoogleDrive("testandroid.8766@gmail.com", "magic@123");
	}
}
