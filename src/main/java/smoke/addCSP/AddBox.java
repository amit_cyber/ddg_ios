package smoke.addCSP;

import org.testng.annotations.Test;

import functions.Set_Driver_Set_Capabilities;
import functions.HomePageFunctions.HomeScreen;
import functions.LoginPage.PinScreen;
import functions.createNew.Add_CSP;
import functions.createNew.CreateNewMenu;

public class AddBox extends Set_Driver_Set_Capabilities {

	HomeScreen HS_Obj = new HomeScreen();
	Add_CSP add_obj = new Add_CSP();
	CreateNewMenu CN_obj = new CreateNewMenu();
	PinScreen ps = new PinScreen();

	@Test(priority = 1)
	public void addCSP() throws InterruptedException {

		ps.EnterPin();
		// Click (+) button
		CN_obj.clickAddButton();

		// Click on Cloud Service button
		CN_obj.addCSP();
	}

	@Test(priority = 2)
	public void addBox() {

		add_obj.addBoxDrive("testcguser@gmail.com", "cybergroup01");

	}
}
